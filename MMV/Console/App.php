<?php

namespace MMV\Console;

use MMV\Console\Exception;
use Throwable;

class App
{
    /**
     * @return mixed
     */
    public function run() {
        if($this->_needHelp()) $this->_showHelp();
        else {
            try {
                if($this->_config['showHelpDefault'] && !count($this->_parseArgv())) $this->_showHelp();
                else {
                    return call_user_func($this->_config['func'], $this->_argv());
                }
            } catch (Exception $e) {
                echo 'Error request: '.$e->getMessage();
                if($this->_config['debug']) {
                    throw $e;
                }
                return 1;
            }
        }
    }

    public static function make(): self {
        $class = get_called_class();
        return new $class ();
    }

    public function func(callable $function): self {
        $this->_config['func'] = $function;
        return $this;
    }

    /**
     * Set argument without name
     */
    public function arg(string $name, bool $required=false): self {
        $this->_config['args'][] = ['name'=>mb_strtolower($name), 'required'=>(int)$required];
        $this->_sortArgs();
        return $this;
    }

    /**
     * Set arguments as flag. See option()
     */
    public function flag(string $name, array $config=[]): self {
        $config['flag'] = 1;
        $this->option($name, $config);
        return $this;
    }

    /**
     * Set arguments as option
     *
     * Config:
     *   flag     - it is flag
     *   required - required option
     *   default  - default value
     *   alt      - alternative name
     *   help     - text description
     *   type     - help about type for it option
     */
    public function option(string $name, array $config=[]): self {
        $config['name'] = $name;
        $this->_config['options'][] = $this->_prepareConfigForOption($config);
        return $this;
    }

    /**
     * Set description. Text before options
     */
    public function text(string $text): self {
        $this->_config['description'] = $text;
        return $this;
    }

    /**
     * Set description. Text after options
     */
    public function help(string $text): self {
        $this->_config['help'] = $text;
        return $this;
    }

    /**
     * If argv is empty to show help
     */
    public function showHelpDefault(bool $mode): self {
        $this->_config['showHelpDefault'] = (int)$mode;
        return $this;
    }

    /**
     * Set flag read multiple val, not required arguments without name
     */
    public function readNoSetArgs(bool $mode): self {
        $this->_config['readNoSetArgs'] = (int)$mode;
        return $this;
    }

    public function debug(bool $mode): self {
        $this->_config['debug'] = (int)$mode;
        return $this;
    }

    //////////////////////////////////////////////////////////////////////////

    /**
     * @var array
     */
    protected $_config = [
        'func'            => null,
        'description'     => null,
        'args'            => [],
        'options'         => [],
        'readNoSetArgs'   => 0,
        'help'            => null,
        'showHelpDefault' => 0,
        'debug'           => 0,
    ];

    protected ?array $_argv = null;

    protected function _argv(): array {
        $result = [];
        $parse = $this->_parseArgv();

        /**
         * Arguments
         */
        $args = $this->_getOnlyArgs($parse);
        $argsNoSet = [];
        if(count($args) < $this->_countRequiredArgs())
            throw new Exception("Not found required arguments.\nSee the help (-h or --help) for more information.");

        $diff = count($this->_config['args']) - count($args);
        if($diff > 0) $args = array_merge($args, array_fill(0, $diff, ''));

        foreach($args as $key => $i) {
            if(isset($this->_config['args'][$key])) {
                $name = $this->_config['args'][$key]['name'];
                $result[$name] = $i;
            }
            else {
                if($this->_config['readNoSetArgs']) {
                    $argsNoSet[] = $i;
                }
            }
        }
        $result['_'] = $argsNoSet;

        /**
         * Options
         */
        if($this->_config['options']) {
            foreach($this->_config['options'] as $opt) {
                $search = $this->_findByName($opt['name'], $parse);
                if($search === null && $opt['alt']) $search = $this->_findByName($opt['alt'], $parse);

                if($opt['required'] && $search === null)
                    throw new Exception("Not found required option ".$this->_buildNameOption($opt['name'])."\nSee the help (-h or --help) for more information.");

                if($opt['flag']) {
                    $result[$opt['name']] = $search ? 1 : 0;
                } else {
                    $val = $search['val'] ?? ($opt['default'] ?? '');
                    $result[$opt['name']] = $val;
                }
            }
        }

        return $result;
    }

    protected function _getOnlyArgs(array $args): array {
        $result = [];
        foreach($args as $i) {
            if(!isset($i['name'])) $result[] = $i['val'];
        }
        return $result;
    }

    protected function _countRequiredArgs(): int {
        $result = 0;
        foreach($this->_config['args'] as $i) {
            if($i['required']) $result++;
        }
        return $result;
    }

    protected function _getScriptName(): string {
        return $_SERVER['argv'][0];
    }

    protected function _getArgv(): array {
        $t = $_SERVER['argv'];
        unset($t[0]);
        return $t;
    }

    protected function _parseArgv(): array {
        if($this->_argv === null) {
            $result = [];
            foreach($this->_getArgv() as $c) {
                if($t = $this->_parseOption($c, '/^-([^-=])(?:=(.*))?$/')) {
                    $r = ['name' => mb_strtolower($t[1])];
                    if(isset($t[2])) $r['val'] = $t[2];
                }
                else if($t = $this->_parseOption($c, '/^--([^-=]+)(?:=(.*))?$/')) {
                    $r = ['name' => mb_strtolower($t[1])];
                    if(isset($t[2])) $r['val'] = $t[2];
                }
                else {
                    $r = ['val'=>$c];
                }
                $result[] = $r;
            }

            $result = $this->_setValueForFlag($result);

            $this->_argv = $this->_reloadOptionWithExtendedRules($result);
        }

        return $this->_argv;
    }

    protected function _setValueForFlag(array $args): array
    {
        foreach($args as $key => $val) {
            if(array_key_exists('name', $val)) {
                $search = $this->_findByName($val['name'], $this->_config['options']);
                if($search && $search['flag']) $args[$key]['val'] = 1;
            }
        }
        return $args;
    }

    /**
     * Example: "script --test value" is equal to "script --test=value"
     * if "--test" is not a flag
     */
    protected function _reloadOptionWithExtendedRules(array $args): array {
        $result = [];

        for($n=count($args)-1; $n>=0; $n--) {
            if(array_key_exists('name', $args[$n])) {
                $result[] = $args[$n];
            } else {
                $prev = $args[$n-1] ?? ['val'=>true];
                if(array_key_exists('val', $prev)) {
                    $result[] = $args[$n];
                } else {
                    $args[$n-1]['val'] = $args[$n]['val'];
                }
            }
        }

        return array_reverse($result);
    }

    protected function _parseOption(string $text, string $pattern): array {
        if(preg_match($pattern, $text, $matches)) {
            return $matches;
        }
        return [];
    }

    protected function _needHelp(): bool {
        return
            ($this->_findByName('h',    $this->_parseArgv()) ||
            $this->_findByName('help', $this->_parseArgv()));
    }

    protected function _findByName(string $name, array $args): ?array {
        foreach($args as $arg) {
            if(isset($arg['name']) && $name == $arg['name']) return $arg;
        }
        return null;
    }

    /**
     * Show help
     */
    protected function _showHelp() {
        echo  $this->_showCommandLine() .
              $this->_showText($this->_config['description'], "\n") .
              $this->_showOptions() .
              $this->_showText($this->_config['help'], "\n\n") .
              "\n";
    }

    protected function _showCommandLine(): string {
        $result = '$ ';
        $result .= pathinfo($this->_getScriptName(), PATHINFO_BASENAME);

        $args = [];
        foreach($this->_config['args'] as $i) {
            if($i['required']) $args[] = '<'.$i['name'].'>';
            else $args[] = '['.$i['name'].']';
        }
        if($args) $result .= ' '.implode(' ', $args);
        if($this->_config['readNoSetArgs']) $result .= ' ...';

        if(count($this->_config['options'])) $result .= ' [OPTIONS]';

        return $this->_mb_wordwrap($result, 78, "\n", true);
    }

    protected function _showText(string $text, string $before=''): string {
        if($text) {
            return $before . $this->_mb_wordwrap($text, 78, "\n", true);
        }
        else return '';
    }

    protected function _addDefaultToHelp(array $options): string {
        $result = [];
        if($options['help']) $result[] = $options['help'];
        if($options['default']) $result[] = "DEFAULT: ".$options['default'];
        return implode("\n", $result);
    }

    protected function _showOptions(): string {
        $result = [];

        foreach($this->_config['options'] as $opt) {
            $result[] = $this->_mergeOptionAndHelp($this->_showOptionCommand($opt),
                $this->_addDefaultToHelp($opt));
        }

        //  -h, --help                 Show help

        if($result) return "\n\n".implode("\n", $result);
        else return '';
    }

    protected function _mergeOptionAndHelp(string $option, string $help): string {
        if($help)
            $help = $this->_indent($this->_mb_wordwrap($help, 49, "\n", true), 29);

        if(mb_strlen($option) > 25) {
            $result = $this->_indent($this->_mb_wordwrap($option, 76, "\n", true), 2);
            if($help) $result .= "\n" . $help;
        } else {
            if($help) {
                $result = substr_replace($help, $option, 2, mb_strlen($option));
            } else {
                $result = $this->_indent($option, 2);
            }
        }

        return $result;
    }

    /**
     * Left indent for text
     */
    public function _indent(string $text, int $num): string {
        $tab = str_repeat(' ', $num);
        $result = explode("\n", $text);
        for($n=0; $n<count($result); $n++) {
            $result[$n] = $tab . $result[$n];
        }

        return implode("\n", $result);
    }

    protected function _showOptionCommand(array $option): string  {
        $result = $this->_buildNameOption($option['name']);
        if($option['alt']) $result .= ', '.$this->_buildNameOption($option['alt']);
        if($option['type']) {
            if($option['required']) $result .= '=<'.$option['type'].'>';
            else $result .= '=['.$option['type'].']';
        }

        return $result;
    }

    protected function _buildNameOption(string $name): string  {
        if(mb_strlen($name) > 1) return '--'.$name;
        else return '-'.$name;
    }

    /**
     * @see See wordwrap in php document
     */
    protected function _mb_wordwrap($str, $width = 75, $break = "\n", $cut = false): string {
        $row = explode($break, $str);
        $result = [];

        for($n=0; $n<count($row); $n++) {
            $t = trim($row[$n]);
            if(!$t) {
                $result[] = $t;
                continue;
            }

            while(mb_strlen($t) > $width) {
                $sub = mb_substr($t, 0, $width);
                $pos = mb_strrpos($sub, ' ');
                if($pos === false) {
                    if($cut) {
                        $result[] = $sub;
                        $t = ltrim(mb_substr($t, $width));
                    } else {
                        $pos = mb_strpos($t, ' ');
                        if($pos == false) {
                            $result[] = $t;
                            $t = '';
                        } else {
                            $result[] = mb_substr($t, 0, $pos);
                            $t = ltrim(mb_substr($t, $pos+1));
                        }
                    }
                } else {
                    $result[] = mb_substr($t, 0, $pos);
                    $t = ltrim(mb_substr($t, $pos+1));
                }
            }

            if($t) $result[] = $t;
        }

        return implode($break, $result);
    }

    protected function _prepareConfigForOption(array $config): array {
        $default = [
            'name'     => 'unknown',
            'flag'     => 0,
            'required' => 0,
            'default'  => '',
            'alt'      => '',
            'help'     => '',
            'type'      => '',
        ];

        foreach($default as $key => $val) {
            if(array_key_exists($key, $config)) $default[$key] = $config[$key];
        }

        return $default;
    }

    protected function _sortArgs() {
        if(count($this->_config['args']) <= 1) return;

        $sort = true;

        while($sort) {
            $sort = false;
            $n = 0;
            while($n < count($this->_config['args']) - 1) {
                if(
                    !$this->_config['args'][$n]['required'] && 
                    $this->_config['args'][$n+1]['required']
                ) {
                    $t = $this->_config['args'][$n];
                    $this->_config['args'][$n] = $this->_config['args'][$n+1];
                    $this->_config['args'][$n+1] = $t;
                    $sort = true;
                }
                $n++;
            }
        }
    }
}
