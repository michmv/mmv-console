# mmv/console

Framework for build console application on `php` language programming.

### example

```php
#!/usr/bin/env php
<?php
include(__DIR__.'/vendor/autoload.php');

$test = MMV\Console\App::make()
    ->func(function(array $argv){ // function for your application
        print_r($argv);
    })
    ->text    ('Description application')
    ->help    ('Manual for you application')
    ->arg     ('var1')        // First required argument
    ->arg     ('var2', false) // Second no required argument
    ->flag    ('a',            [ 'alt' => 'action'
                               , 'help' => 'Description how to use this flag. The text can be of any length.'
                               ])
    ->option  ('bug',          [ 'help' => 'Description.'
                               , 'default' => 'Bugs will not pass'
                               ])
    ->option  ('modification', [ 'alt' => 'big-modification'
                               , 'help' => 'Description.'
                               ])
    ->option  ('target',       [ 'type' => 'sting'
                               , 'required' => 1
                               , 'help' => 'Required option'
                               ])
    ->option  ('show',         [ 'alt' => 's'
                               , 'type' => 'int'
                               , 'help' => 'Not Required option.'
                               ])
    ->showHelpDefault(true) // Show help if there is no parameter
    ->readNoSetArgs(true) // many optional arguments
    ->debug(true)
    ;

$test->run(); // run application
```

**$ example --help**

```
$ console.php [var1] [var2] ... [OPTIONS]
Description application

  -a, --action               Description how to use this flag. The text can
                             be of any length.
  --bug                      Description.
                             DEFAULT: Bugs will not pass
  --modification, --big-modification
                             Description.
  --target=<sting>           Required option
  --show, -s=[int]           Not Required option.

Manual for you application
```

**$ example var1 val2 val3 var4 --action --target value -s=10**

```
Array
(
    [var1] => var1
    [var2] => val2
    [_] => Array
        (
            [0] => val3
            [1] => var4
        )

    [a] => 1
    [bug] => Bugs will not pass
    [modification] => 
    [target] => value
    [show] => 10
)
```
