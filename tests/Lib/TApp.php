<?php

namespace Tests;

use MMV\Console\App;

class TApp extends App
{
    public function _mb_wordwrap($str, $width = 75, $break = "\n", $cut = false): string
    {
        return parent::_mb_wordwrap($str, $width, $break, $cut);
    }

    public function _setArgv($arr) {
        unset($arr[0]);
        $this->__argv = $arr;
    }

    protected function _getArgv(): array {
        return $this->__argv;
    }

    protected $__argv = [];
}
