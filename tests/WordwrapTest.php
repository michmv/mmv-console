<?php

include_once(__DIR__.'/Lib/TApp.php');

use \PHPUnit\Framework\TestCase;

class WordwrapTest extends TestCase
{
    public function getApp()
    {
        return new Tests\TApp();
    }

    // -------------------------------------------------------------------------

    public function testLongString()
    {
        $test = $this->getApp();
        $string = 'Длинная строка на русском языке.';

        $result = $test->_mb_wordwrap($string, 20);

        $this->assertEquals("Длинная строка на\nрусском языке.", $result);
    }

    public function testLongStringEnter()
    {
        $test = $this->getApp();
        $string = "Длинная строка на русском\n языке.";

        $result = $test->_mb_wordwrap($string, 20);

        $this->assertEquals("Длинная строка на\nрусском\nязыке.", $result);
    }

    public function testLongStringTwoEnter()
    {
        $test = $this->getApp();
        $string = "Длинная строка на русском\n\n языке.";

        $result = $test->_mb_wordwrap($string, 20);

        $this->assertEquals("Длинная строка на\nрусском\n\nязыке.", $result);
    }

    public function testLongStringLastSpace()
    {
        $test = $this->getApp();
        $string = "Длинная строка на русском\n языке. ";

        $result = $test->_mb_wordwrap($string, 20);

        $this->assertEquals("Длинная строка на\nрусском\nязыке.", $result);
    }

    public function testLongStringSpaceBeforeEnter()
    {
        $test = $this->getApp();
        $string = "Длинная строка на русском \n языке.";

        $result = $test->_mb_wordwrap($string, 20);

        $this->assertEquals("Длинная строка на\nрусском\nязыке.", $result);
    }
}