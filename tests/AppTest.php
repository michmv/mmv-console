<?php

include_once(__DIR__.'/Lib/TApp.php');

use \PHPUnit\Framework\TestCase;

class AppTest extends TestCase
{
    public function result($array) {
        return $array;
    }

    public function getApp(): Tests\TApp
    {
        return Tests\TApp::make()->func([$this, 'result']);
    }

    public function testNoNameArgs()
    {
        $test = $this->getApp();
        $test->_setArgv(['console.php', 'val1', 'val2', 'val3']);
        $test->arg('v1');

        $this->assertEquals(['v1'=>'val1', '_'=>[]], $test->run());
    }

    public function testNoNameArgsNoSetArgs()
    {
        $test = $this->getApp();
        $test->_setArgv(['console.php', 'val1', 'val2', 'val3']);
        $test->arg('v1')->readNoSetArgs(true);

        $this->assertEquals(['v1'=>'val1', '_'=>['val2', 'val3']], $test->run());
    }

    public function testOnlyNoSetArgs()
    {
        $test = $this->getApp();
        $test->_setArgv(['console.php', 'val1', 'val2', 'val3']);
        $test->readNoSetArgs(true);

        $this->assertEquals(['_'=>['val1', 'val2', 'val3']], $test->run());
    }

    public function testArgsSort()
    {
        $test = $this->getApp();
        $test->_setArgv(['console.php', 'val1', 'val2', 'val3']);
        $test->arg('v1', true)->arg('v3', false)->arg('v2', true);

        $this->assertEquals(['v1'=>'val1', 'v2'=>'val2', 'v3'=>'val3', '_'=>[]], $test->run());
    }

    public function testArgsRequired()
    {
        $test = $this->getApp();
        $test->_setArgv(['console.php']);
        $test->arg('v1', true);

        ob_start();
        $test->run();
        $result = ob_get_clean();

        $this->assertEquals("Error request: Not found required arguments.\nSee the help (-h or --help) for more information.", $result);
    }

    public function testArgsNoRequired()
    {
        $test = $this->getApp();
        $test->_setArgv(['console.php']);
        $test->arg('v1', false);

        $this->assertEquals(['v1'=>'', '_'=>[]], $test->run());
    }

    public function testFlagNoExists()
    {
        $test = $this->getApp();
        $test->_setArgv(['console.php']);
        $test->flag('a');

        $this->assertEquals(['a'=>0, '_'=>[]], $test->run());
    }

    public function testFlagExists()
    {
        $test = $this->getApp();
        $test->_setArgv(['console.php', '-a']);
        $test->flag('a');

        $this->assertEquals(['a'=>1, '_'=>[]], $test->run());
    }

    public function testFlagExistsWithValue()
    {
        $test = $this->getApp();
        $test->_setArgv(['console.php', '-a=value']);
        $test->flag('a');

        $this->assertEquals(['a'=>1, '_'=>[]], $test->run());
    }

    public function testFlagExistsWithArgs()
    {
        $test = $this->getApp();
        $test->_setArgv(['console.php', '-a', 'val1']);
        $test->flag('a')->arg('v1');

        $this->assertEquals(['a'=>1, 'v1'=>'val1', '_'=>[]], $test->run());
    }

    public function testOptionNoExists()
    {
        $test = $this->getApp();
        $test->_setArgv(['console.php']);
        $test->option('v');

        $this->assertEquals(['v'=>'', '_'=>[]], $test->run());
    }

    public function testOptionNoExistsDefault()
    {
        $test = $this->getApp();
        $test->_setArgv(['console.php']);
        $test->option('v', ['default'=>'string']);

        $this->assertEquals(['v'=>'string', '_'=>[]], $test->run());
    }

    public function testOptionExists()
    {
        $test = $this->getApp();
        $test->_setArgv(['console.php', '--v1', 'val1', '--v2=val2']);
        $test->option('v1')->option('v2');

        $this->assertEquals(['v1'=>'val1', 'v2'=>'val2', '_'=>[]], $test->run());
    }

    public function testOptionExistsEmpty()
    {
        $test = $this->getApp();
        $test->_setArgv(['console.php', 'val2', '--v1=', 'val3']);
        $test->option('v1', ['default'=>'val1'])->arg('v2')->arg('v3');

        $this->assertEquals(['v1'=>'', 'v2'=>'val2', 'v3'=>'val3', '_'=>[]], $test->run());
    }

    public function testOptionRequired()
    {
        $test = $this->getApp();
        $test->_setArgv(['console.php']);
        $test->option('v1', ['required'=>1]);

        ob_start();
        $test->run();
        $result = ob_get_clean();

        $this->assertEquals("Error request: Not found required option --v1\nSee the help (-h or --help) for more information.", $result);
    }

    public function testOptionRequiredEmpty()
    {
        $test = $this->getApp();
        $test->_setArgv(['console.php', '--v1']);
        $test->option('v1', ['required'=>1]);

        $this->assertEquals(['v1'=>'', '_'=>[]], $test->run());
    }
}
